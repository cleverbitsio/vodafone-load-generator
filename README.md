To run the default main file (com.cleverbits.app.App)
```java -jar target/quick-start-1.0-SNAPSHOT.jar``` 

To run any other - you need to reference the fully qualified class name
```java -cp target/quick-start-1.0-SNAPSHOT.jar com.cleverbits.basics.ReadCassandraVersion```
```java -cp target/quick-start-1.0-SNAPSHOT.jar com.cleverbits.basics.CreateAndPopulateKeyspace```
```java -cp target/quick-start-1.0-SNAPSHOT.jar com.cleverbits.basics.ReadTopologyAndSchemaMetadata```
```java -cp target/quick-start-1.0-SNAPSHOT.jar com.cleverbits.basics.CassandraLoadGenerator```
```java -cp target/quick-start-1.0-SNAPSHOT.jar com.cleverbits.basics.CassandraLoadGenerator -w 100 -n "node.c.vodafone-cassandra-poc.internal,node2.c.vodafone-cassandra-poc.internal"```
```java -cp target/quick-start-1.0-SNAPSHOT.jar com.cleverbits.basics.CassandraLoadGenerator -w 1000 -n "node.c.vodafone-cassandra-poc.internal,node2.c.vodafone-cassandra-poc.internal,node3.c.vodafone-cassandra-poc.internal" -r```
