package com.cleverbits.basics;

import com.datastax.driver.core.*;

import java.time.format.DateTimeFormatter;
import java.util.*;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.MoreExecutors;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import org.apache.commons.math.stat.StatUtils;


/**
 * Creates a keyspace and tables, and loads some data into them.
 * <p/>
 * Preconditions:
 * - a Cassandra cluster is running and accessible through the contacts points identified by CONTACT_POINTS and PORT.
 * <p/>
 * Side effects:
 * - creates a new keyspace "simplex" in the cluster. If a keyspace with this name already exists, it will be reused;
 * - creates two tables "simplex.songs" and "simplex.playlists". If they exist already, they will be reused;
 * - inserts a row in each table.
 *
 * @see <a href="http://datastax.github.io/java-driver/manual/">Java driver online manual</a>
 * <p>
 * java -cp target/quick-start-1.0-SNAPSHOT.jar com.cleverbits.basics.CassandraLoadGenerator
 */
public class CassandraLoadGenerator {

    static String[] CONTACT_POINTS;
    //static String[] CONTACT_POINTS = {"127.0.0.1"};
    static List<String> nodes;
    static int PORT = 9042;

    private static String[] args = null;
    private static Options options = new Options();
    private static int concurrentWrites = 30;
    private static boolean reads_enabled = false;
    private static long startTime = System.currentTimeMillis();


    public static void main(String[] args) {

        args = args;
        options.addOption("h", "help", false, "show help.");
        options.addOption("n", "nodes", true, "comma separated cassandra nodes");
        options.addOption("w", "writes_per_second", true, "Writes per second.");
        options.addOption("r", "reads_enabled", false, "Reads enabled.");
        CommandLineParser parser = new BasicParser();

        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);

            if (cmd.hasOption("h"))
                help();

            if (cmd.hasOption("w") && cmd.hasOption("n")) {
                System.out.println("Using cli argument -w=" + cmd.getOptionValue("w"));
                // Whatever you want to do with the setting goes here
                concurrentWrites = Integer.parseInt(cmd.getOptionValue("w"));
                System.out.println("Using cli argument -n=" + cmd.getOptionValue("n"));
                String nodesString = cmd.getOptionValue("n");
                System.out.println("nodesString = " + nodesString);
                CONTACT_POINTS = nodesString.split(",", -1);
                System.out.println(CONTACT_POINTS);
                System.out.println(concurrentWrites);
            }
            else {
                System.out.println("Missing w option or n option");
                help();
            }

            if(cmd.hasOption("r")) {
                System.out.println("reads_enabled = true");
                reads_enabled = true;
            }
            else {
                System.out.println("reads_enabled = false");
            }

        } catch (ParseException e) {
            System.out.println("Failed to parse comand line properties " + e);
            help();
        }

        CassandraLoadGenerator client = new CassandraLoadGenerator();

        try {
            client.connect(CONTACT_POINTS, PORT);
            client.createSchema();
            client.loadData();
        } finally {
            client.close();
        }
    }

    private static void help() {
        // This prints out some help
        HelpFormatter formater = new HelpFormatter();

        formater.printHelp("Main", options);
        System.exit(0);
    }

    private Cluster cluster;
    private Session session;

    /**
     * Initiates a connection to the cluster
     * specified by the given contact point.
     *
     * @param contactPoints the contact points to use.
     * @param port          the port to use.
     */
    public void connect(String[] contactPoints, int port) {

        cluster = Cluster.builder()
                .addContactPoints(contactPoints).withPort(port)
                .build();

        System.out.printf("Connected to cluster: %s%n", cluster.getMetadata().getClusterName());

        session = cluster.connect();
    }

    /**
     * Creates the schema (keyspace) and tables
     * for this example.
     */
    public void createSchema() {

        session.execute("CREATE KEYSPACE IF NOT EXISTS vodafone WITH replication " +
                "= {'class':'SimpleStrategy', 'replication_factor':2};");

//            session.execute ("CREATE TABLE IF NOT EXISTS vodafone.write_rate_counter (" +
//                                "start_d_t timestamp," +
//                                "counter_value counter," +
//                                "primary key (start_d_t));"
//                            );

        session.execute("CREATE TABLE IF NOT EXISTS vodafone.counters(" +
                "counter_name text," +
                "counter_value counter," +
                "primary key (counter_name));"
        );

        session.execute(
                "CREATE TABLE IF NOT EXISTS vodafone.load (" +
                        " r_p_msisdn_code int," +
                        " r_p_msc text," +
                        "  r_p_imsi_code int," +
                        "  zpcode int," +
                        "  actual_volume int," +
                        "  r_p_imei text," +
                        "  call_dest text," +
                        "  credit_clicks int," +
                        "  o_p_number text," +
                        "  ttcode int," +
                        "  r_p_ms_class_mark text," +
                        "  start_d_t timestamp," +
                        "  twcode int," +
                        "  rated_clicks int," +
                        "  sp_code int," +
                        "  r_p_sim_id int," +
                        "  facility_usage text," +
                        "  aoc_amount int," +
                        "  tzcode int," +
                        "  r_p_cgi text," +
                        "  r_p_np text," +
                        "  action_code text," +
                        "  tm_version int," +
                        "  umcode int," +
                        "  rated_volume int," +
                        "  rtx_lfnr int," +
                        "  sncode int," +
                        "  trac_start_d_t text," +
                        "  rtx_type text," +
                        "  original_start_d_t timestamp," +
                        "  rtx_sqn int," +
                        "  r_p_ton text," +
                        "  gvcode int," +
                        "  o_p_cgi text," +
                        "  termination_ind int," +
                        "  r_p_mccode int," +
                        "  rtx_sub_lfnr int," +
                        "  r_p_customer_id int," +
                        "  r_p_mcind int," +
                        "  bc int," +
                        "  call_type int," +
                        "  rated_flat_amount int," +
                        "  bill_status text," +
                        "  aoc_ind text," +
                        "  gsm_pi text," +
                        "  market_id text," +
                        "  plcode int," +
                        "  eccode int," +
                        "  roamer_ind text," +
                        "  rounded_volume int," +
                        "  delete_after_bill text," +
                        "  tmcode int," +
                        "  r_p_imsi int," +
                        "  vpn_ind text," +
                        "  tariff_class text," +
                        "  call_reference int," +
                        "  cause_for_term text," +
                        "  pstn_id int," +
                        "  routing_category text," +
                        "  rated_flat_amount_euro double," +
                        "  cdr_indicator text," +
                        "  r_p_number text," +
                        "  visited_cc_code int," +
                        "  data_volume int," +
                        "  o_p_rgn int," +
                        "  roam_rerating_ind int," +
                        "  inputed_to_primary text," +
                        "  primary_object_id int," +
                        "  origin_zpcode int," +
                        "  dialled_digits text," +
                        "  circe_product text," +
                        "  counter text," +
                        "  bonus text," +
                        "  wisp text," +
                        "  hot_spot text," +
                        "  authentication_type text," +
                        "  charging_id int," +
                        "  ggsn_address text," +
                        "  bearer int," +
                        "  bit_rate int," +
                        "  qosnegotiated text," +
                        "  promo_elab_date timestamp," +
                        "  primary_gn_sim_id int," +
                        "  connected_sim_id_from_gn int," +
                        "  number_pm_pulses int," +
                        "  apn_ind int," +
                        "  delay_call text," +
                        "  rated_flat_amount_euro_tm double," +
                        " primary key ((r_p_sim_id),start_d_t)" + //may want to make the partiton key more unique to prevent future wide row limit
                        ") " +
                        "WITH CLUSTERING ORDER BY (start_d_t ASC)" +
                        " AND read_repair_chance = 0.0" +          //recommended for TWCS
                        " AND gc_grace_seconds = 60" +             //time to wait before a tombstone can be purged
                        " AND default_time_to_live = 0" +          //TTL of records in seconds. 0 means we don't have a TTL
                        " AND compaction = {'compaction_window_size': '1', " +
                        "'compaction_window_unit': 'MINUTES', " +
                        // "AND compaction = {'compaction_window_size': '1', " +
                        // "'compaction_window_unit': 'DAYS', " +
                        "'class': 'org.apache.cassandra.db.compaction.TimeWindowCompactionStrategy'}"
        );
    }

    /**
     * Inserts data into the tables.
     */
    public void loadData() {


        //        if(concurrentWrites<18)
        //            concurrentWrites = 18;

        //18 concurrent writes p/s
        ResultSet distinctCustomers = session.execute("SELECT DISTINCT r_p_sim_id FROM vodafone.prod LIMIT " + concurrentWrites);
        //Removing all limits with 1 minute TWCS makes queries timeout!!
        //ResultSet distinctCustomers = session.execute("SELECT DISTINCT r_p_sim_id FROM vodafone.prod");
        //ResultSet distinctCustomers = session.execute("SELECT DISTINCT r_p_sim_id FROM vodafone.prod LIMIT 100");
        List<Row> distinctCustomerRows = new ArrayList<Row>();
        for (Row row : distinctCustomers) {
            distinctCustomerRows.add(row);
        }
        List<Integer> customers = new ArrayList<Integer>();
        List<Row> customerRecords = new ArrayList<Row>();

        System.out.println("customers size = " + customers.size());
        System.out.println("concurrentWrites = " + concurrentWrites);

        while (customers.size() < concurrentWrites) {
            for (Row row : distinctCustomerRows) {
                customers.add(row.getInt("r_p_sim_id"));
                if (customers.size() == concurrentWrites) break;
            }
        }
        System.out.println("customers size = " + customers.size());

        //Read customer rows into List
        for (int x = 0; x < customers.size(); x++) {
            ResultSet customerRecord = session.execute("SELECT * FROM vodafone.prod where r_p_sim_id = " + customers.get(x) + " LIMIT 1"); //One read
            customerRecords.add(customerRecord.one());
        }
        System.out.println("customerRecords size = " + customerRecords.size());
        create_load(customerRecords);
        return;


    }

    private void create_load(List<Row> customerRecords) {
        //Continously write - based on customer rows
        while (true) {
            //Go through each customer record
            //for each row (there will only be one due to LIMIT 1) in the customer - write it to cassandra

            final int[] histogram = new int[5];
            final List<Long> write_latencies = new ArrayList<Long>();
            final List<Long> read_latencies = new ArrayList<Long>();
            final AtomicInteger numberOfWrites = new AtomicInteger(1);
            final AtomicInteger numberOfReads = new AtomicInteger(0);

            for (Row row : customerRecords) {                //one read and one write
                final LocalDateTime write_start_time = LocalDateTime.now();
                final Row current_row = row;
                ResultSetFuture future = session.executeAsync(
                "INSERT INTO vodafone.load" +
                        "( " +
                        "  r_p_sim_id, " +
                        "  start_d_t, " +
                        "  r_p_msisdn_code," +
                        "  r_p_msc," +
                        "  r_p_imsi_code," +
                        "  zpcode," +
                        "  actual_volume," +
                        "  r_p_imei," +
                        "  call_dest," +
                        "  credit_clicks," +
                        "  o_p_number," +
                        "  ttcode," +
                        "  r_p_ms_class_mark," +
                        "  twcode," +
                        "  rated_clicks," +
                        "  sp_code," +
                        "  facility_usage," +
                        "  aoc_amount," +
                        "  tzcode," +
                        "  r_p_cgi," +
                        "  r_p_np," +
                        "  action_code," +
                        "  tm_version," +
                        "  umcode," +
                        "  rated_volume," +
                        "  rtx_lfnr," +
                        "  sncode," +
                        "  trac_start_d_t," +
                        "  rtx_type," +
                        "  original_start_d_t," +
                        "  rtx_sqn," +
                        "  r_p_ton," +
                        "  gvcode," +
                        "  o_p_cgi," +
                        "  termination_ind," +
                        "  r_p_mccode," +
                        "  rtx_sub_lfnr," +
                        "  r_p_customer_id," +
                        "  r_p_mcind," +
                        "  bc," +
                        "  call_type," +
                        "  rated_flat_amount," +
                        "  bill_status," +
                        "  aoc_ind," +
                        "  gsm_pi," +
                        "  market_id," +
                        "  plcode," +
                        "  eccode," +
                        "  roamer_ind," +
                        "  rounded_volume," +
                        "  delete_after_bill," +
                        "  tmcode," +
                        "  r_p_imsi," +
                        "  vpn_ind," +
                        "  tariff_class," +
                        "  call_reference," +
                        "  cause_for_term," +
                        "  pstn_id," +
                        "  routing_category," +
                        "  rated_flat_amount_euro," +
                        "  cdr_indicator," +
                        "  r_p_number," +
                        "  visited_cc_code," +
                        "  data_volume," +
                        "  o_p_rgn," +
                        "  roam_rerating_ind," +
                        "  inputed_to_primary," +
                        "  primary_object_id," +
                        "  origin_zpcode," +
                        "  dialled_digits," +
                        "  circe_product," +
                        "  counter," +
                        "  bonus," +
                        "  wisp," +
                        "  hot_spot," +
                        "  authentication_type," +
                        "  charging_id," +
                        "  ggsn_address," +
                        "  bearer," +
                        "  bit_rate," +
                        "  qosnegotiated," +
                        "  promo_elab_date," +
                        "  primary_gn_sim_id," +
                        "  connected_sim_id_from_gn," +
                        "  number_pm_pulses," +
                        "  apn_ind," +
                        "  delay_call," +
                        "  rated_flat_amount_euro_tm" +
                        "  )" +
                        "  VALUES (" +
                        row.getInt("r_p_sim_id") + "," +
                        //"'" + row.getTimestamp("start_d_t") + "'," +
                        "'" + write_start_time + "'," +
                        row.getInt("r_p_msisdn_code") + "," +
                        "'" + row.getString("r_p_msc") + "'," +
                        row.getInt("r_p_imsi_code") + "," +
                        row.getInt("zpcode") + "," +
                        row.getInt("actual_volume") + "," +
                        "'" + row.getString("r_p_imei") + "'," +
                        "'" + row.getString("call_dest") + "'," +
                        row.getInt("credit_clicks") + "," +
                        "'" + row.getString("o_p_number") + "'," +
                        row.getInt("ttcode") + "," +
                        "'" + row.getString("r_p_ms_class_mark") + "'," +
                        row.getInt("twcode") + "," +
                        row.getInt("rated_clicks") + "," +
                        row.getInt("sp_code") + "," +
                        "'" + row.getString("facility_usage") + "'," +
                        row.getInt("aoc_amount") + "," +
                        row.getInt("tzcode") + "," +
                        "'" + row.getString("r_p_cgi") + "'," +
                        "'" + row.getString("r_p_np") + "'," +
                        "'" + row.getString("action_code") + "'," +
                        row.getInt("tm_version") + "," +
                        row.getInt("umcode") + "," +
                        row.getInt("rated_volume") + "," +
                        row.getInt("rtx_lfnr") + "," +
                        row.getInt("sncode") + "," +
                        "'" + row.getString("trac_start_d_t") + "'," +
                        "'" + row.getString("rtx_type") + "'," +
                        //row.getTimestamp("original_start_d_t") + "," +
                        "'" + write_start_time + "'," +
                        row.getInt("rtx_sqn") + "," +
                        "'" + row.getString("r_p_ton") + "'," +
                        row.getInt("gvcode") + "," +
                        "'" + row.getString("o_p_cgi") + "'," +
                        row.getInt("termination_ind") + "," +
                        row.getInt("r_p_mccode") + "," +
                        row.getInt("rtx_sub_lfnr") + "," +
                        row.getInt("r_p_customer_id") + "," +
                        row.getInt("r_p_mcind") + "," +
                        row.getInt("bc") + "," +
                        row.getInt("call_type") + "," +
                        row.getInt("rated_flat_amount") + "," +
                        "'" + row.getString("bill_status") + "'," +
                        "'" + row.getString("aoc_ind") + "'," +
                        "'" + row.getString("gsm_pi") + "'," +
                        "'" + row.getString("market_id") + "'," +
                        row.getInt("plcode") + "," +
                        row.getInt("eccode") + "," +
                        "'" + row.getString("roamer_ind") + "'," +
                        row.getInt("rounded_volume") + "," +
                        "'" + row.getString("delete_after_bill") + "'," +
                        row.getInt("tmcode") + "," +
                        row.getInt("r_p_imsi") + "," +
                        "'" + row.getString("vpn_ind") + "'," +
                        "'" + row.getString("tariff_class") + "'," +
                        row.getInt("call_reference") + "," +
                        "'" + row.getString("cause_for_term") + "'," +
                        row.getInt("pstn_id") + "," +
                        "'" + row.getString("routing_category") + "'," +
                        row.getDouble("rated_flat_amount_euro") + "," +
                        "'" + row.getString("cdr_indicator") + "'," +
                        "'" + row.getString("r_p_number") + "'," +
                        row.getInt("visited_cc_code") + "," +
                        row.getInt("data_volume") + "," +
                        row.getInt("o_p_rgn") + "," +
                        row.getInt("roam_rerating_ind") + "," +
                        "'" + row.getString("inputed_to_primary") + "'," +
                        row.getInt("primary_object_id") + "," +
                        row.getInt("origin_zpcode") + "," +
                        "'" + row.getString("dialled_digits") + "'," +
                        "'" + row.getString("circe_product") + "'," +
                        "'" + row.getString("counter") + "'," +
                        "'" + row.getString("bonus") + "'," +
                        "'" + row.getString("wisp") + "'," +
                        "'" + row.getString("hot_spot") + "'," +
                        "'" + row.getString("authentication_type") + "'," +
                        row.getInt("charging_id") + "," +
                        "'" + row.getString("ggsn_address") + "'," +
                        row.getInt("bearer") + "," +
                        row.getInt("bit_rate") + "," +
                        "'" + row.getString("qosnegotiated") + "'," +
                        //"'" + row.getTimestamp("promo_elab_date") + "'," +
                        "'" + write_start_time + "'," +
                        row.getInt("primary_gn_sim_id") + "," +
                        row.getInt("connected_sim_id_from_gn") + "," +
                        row.getInt("number_pm_pulses") + "," +
                        row.getInt("apn_ind") + "," +
                        "'" + row.getString("delay_call") + "'," +
                        row.getDouble("rated_flat_amount_euro_tm") +
                        ");");
                Futures.addCallback(future,
                        new FutureCallback<ResultSet>() {
                            public void onSuccess(ResultSet result) {
                                numberOfWrites.addAndGet(1);
                                LocalDateTime write_end_time = LocalDateTime.now();
                                long latency = java.time.Duration.between(write_start_time, write_end_time).toMillis();
                                if(!Double.isNaN((double) latency))
                                    write_latencies.add(latency); //write latency

                                //read back here
                                if(reads_enabled) {
                                    final LocalDateTime read_start_time = write_end_time;
                                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                                    ResultSetFuture read_future = session.executeAsync("SELECT * FROM vodafone.load WHERE r_p_sim_id =" + current_row.getInt("r_p_sim_id") +
                                            " AND start_d_t >= '" + write_start_time.format(formatter) + "' AND start_d_t <= '" + read_start_time.format(formatter) + "';");
                                    Futures.addCallback(read_future,
                                            new FutureCallback<ResultSet>() {
                                                public void onSuccess(ResultSet result) {
                                                    numberOfReads.addAndGet(1);
                                                    LocalDateTime read_end_time = LocalDateTime.now();
                                                    long latency = java.time.Duration.between(read_start_time, read_end_time).toMillis();
                                                    if(!Double.isNaN((double) latency))
                                                        read_latencies.add(latency); //write latency
                                                    //System.out.println(result.one()); //confirmed through console that this returns results (at times)
                                                }

                                                public void onFailure(Throwable t) {
                                                }
                                            },
                                            MoreExecutors.sameThreadExecutor()
                                    );
                                }
                            }

                            public void onFailure(Throwable t) {
                                System.out.println("Error while writing: " + t.getMessage());
                            }
                        },
                        MoreExecutors.sameThreadExecutor()
                );

            }
            //Update counter for writes
            session.executeAsync("UPDATE vodafone.counters SET counter_value = counter_value + " + numberOfWrites.get() + " WHERE counter_name='write_rate_counter';");
            session.executeAsync("UPDATE vodafone.counters SET counter_value = counter_value + " + numberOfReads.get() + " WHERE counter_name='read_rate_counter';");

            try {
                //printHistogram(histogram);
                printStatsMyStats(write_latencies, "write");
                printStatsMyStats(read_latencies, "read");
                printUptime();
            } catch (NullPointerException npe) {

            }
            //After loop through the said number of customers - we sleep for 1 second
            try {
                Thread.sleep(1000);
                resetConsole();
            } catch (InterruptedException e) {

            }
        }
    }

    private void update_histogram(long latency, int[] histogram) {
        System.out.println("Latency = " + latency);
        if(latency < 6) {
            histogram[0]++;
        }
        else if(latency > 5 && latency <11) {
            histogram[1]++;
        }
        else if(latency > 10 && latency <51) {
            histogram[2]++;
        }
        else if(latency > 50 && latency <101) {
            histogram[3]++;
        }
        else {
            histogram[4]++;
        }
    }

    /**
     * Closes the session and the cluster.
     */
    public void close() {
        session.close();
        cluster.close();
    }

    private static void printHistogram(int[] array) {
        for (int range = 0; range < array.length; range++) {
            String label;
            if (range == 0) label = "   0-5 milliseconds => ";
            else if (range == 1) label = "  6-10 milliseconds => ";
            else if (range == 3) label = " 10-50 milliseconds => ";
            else if (range == 4) label = "51-100 milliseconds => ";
            else label = "  >100 milliseconds => ";
            System.out.println(label + convertToStars(array[range]));
        }
    }

    public static double max_write_latency = 1; //start low to get the correct max_write_latency
    public static double min_write_latency = 100; //start high to get the correct min_write_latency
    public static List<Double> fifty_write_latency = new ArrayList<Double>();
    public static List<Double> seventyfive_write_latency = new ArrayList<Double>();
    public static List<Double> ninetyfive_write_latency = new ArrayList<Double>();
    public static List<Double> ninetynine_write_latency = new ArrayList<Double>();

    public static double max_read_latency = 1; //start low to get the correct max_write_latency
    public static double min_read_latency = 100; //start high to get the correct min_write_latency
    public static List<Double> fifty_read_latency = new ArrayList<Double>();
    public static List<Double> seventyfive_read_latency = new ArrayList<Double>();
    public static List<Double> ninetyfive_read_latency = new ArrayList<Double>();
    public static List<Double> ninetynine_read_latency = new ArrayList<Double>();

    public static Object lockObject = new Object();
    private static void printStatsMyStats(List<Long> values, String operation) {

        synchronized (lockObject) {
            if (values != null) {
                System.out.println("values.size() = " + values.size());
                System.out.println("in printStatsMyStats");
                double[] target = new double[values.size()];
                for (int i = 0; i < target.length; i++) {
                    target[i] = values.get(i).doubleValue();  // java 1.4 style
                    // or:
                    target[i] = values.get(i);                // java 1.5+ style (outboxing)
                }


                if(operation == "write") {
                    min_write_latency = Math.min(StatUtils.min(target), min_write_latency);
                    max_write_latency = Math.max(StatUtils.max(target), max_write_latency);

                    double fifty_percentile_current_run = StatUtils.percentile(target, 50);
                    fifty_write_latency.add(fifty_percentile_current_run);
                    double fiftyPercentile_over_all_runs = StatUtils.percentile(toPrimitive(fifty_write_latency.toArray(new Double[fifty_write_latency.size()])), 45);//45 KEEPS LATENCIES CLOSE TO CASSANDRA STATS FOR 50th %tile

                    double seventyfive_percentile_current_run = StatUtils.percentile(target, 75);
                    seventyfive_write_latency.add(seventyfive_percentile_current_run);
                    double seventyfivePercentile_over_all_runs = StatUtils.percentile(toPrimitive(seventyfive_write_latency.toArray(new Double[seventyfive_write_latency.size()])), 45);//45 KEEPS LATENCIES CLOSE TO CASSANDRA STATS FOR 75th %tile

                    double ninetyfive_percentile_current_run = StatUtils.percentile(target, 95);
                    ninetyfive_write_latency.add(ninetyfive_percentile_current_run);
                    //30 is pretty good for 2500, lets try 35
                    //35 seems to have improved for 2500 for matching with datadog, lets try 40
                    //40 seems to have improved for 2500 for matching with datadog, lets try 45
                    //45 seems to have improved for 2500 for matching with datadog - perfect with reads, but off a little w/o reads, a little off with writes only, lets try 50
                    //50 seems to have improved for 2500 for matching with datadog - perfect with reads, but off a little w/o reads, I assume we could have a diff config w/o reads - i.e. 35-60?
                    double ninetyfivePercentile_over_all_runs;
                    if(!reads_enabled) {
                        System.out.println("reads_enabled = " + reads_enabled );
                        ninetyfivePercentile_over_all_runs = StatUtils.percentile(toPrimitive(ninetyfive_write_latency.toArray(new Double[ninetyfive_write_latency.size()])), 50); //40 KEEPS LATENCIES CLOSE TO CASSANDRA STATS FOR 95th %tile
                    }
                    else {
                        System.out.println("reads_enabled = " + reads_enabled );
                        ninetyfivePercentile_over_all_runs = StatUtils.percentile(toPrimitive(ninetyfive_write_latency.toArray(new Double[ninetyfive_write_latency.size()])), 50); //50 KEEPS LATENCIES CLOSE TO CASSANDRA STATS FOR 95th %tile
                    }

                    //99 is pretty good for 2500 with mixed reads and writes - but need a write only config, lets try 45 without reads and 99 with reads
                    double ninetynine_percentile_current_run = StatUtils.percentile(target, 99);
                    ninetynine_write_latency.add(ninetynine_percentile_current_run);
                    double ninetyninePercentile_over_all_runs;
                    if(!reads_enabled) {
                        System.out.println("reads_enabled = " + reads_enabled );
                        ninetyninePercentile_over_all_runs = StatUtils.percentile(toPrimitive(ninetynine_write_latency.toArray(new Double[ninetynine_write_latency.size()])), 45); //45 KEEPS LATENCIES CLOSE TO CASSANDRA STATS FOR 99th %tile
                    }
                    else {
                        System.out.println("reads_enabled = " + reads_enabled );
                        ninetyninePercentile_over_all_runs = StatUtils.percentile(toPrimitive(ninetynine_write_latency.toArray(new Double[ninetynine_write_latency.size()])), 99); //99 KEEPS LATENCIES CLOSE TO CASSANDRA STATS FOR 99th %tile
                    }

                    System.out.println("min_write_latency: " + min_write_latency);
                    System.out.println("max_write_latency: " + max_write_latency);
                    System.out.println(operation + "50th %tile over all runs: " + fiftyPercentile_over_all_runs + " 50th %tile for current run: " + fifty_percentile_current_run);
                    System.out.println(operation + "75th %tile over all runs: " + seventyfivePercentile_over_all_runs + " 75th %tile for current run: " + seventyfive_percentile_current_run);
                    System.out.println(operation + "95th %tile over all runs: " + ninetyfivePercentile_over_all_runs + " 95th %tile for current run: " + ninetyfive_percentile_current_run);
                    System.out.println(operation + "99th %tile over all runs: " + ninetyninePercentile_over_all_runs + " 99th %tile for current run: " + ninetynine_percentile_current_run);
                }
                else {
                    min_read_latency = Math.min(StatUtils.min(target), min_read_latency);
                    max_read_latency = Math.max(StatUtils.max(target), max_read_latency);

                    double fifty_percentile_current_run = StatUtils.percentile(target, 50);
                    fifty_read_latency.add(fifty_percentile_current_run);
                    double fiftyPercentile_over_all_runs = StatUtils.percentile(toPrimitive(fifty_read_latency.toArray(new Double[fifty_read_latency.size()])), 45);//45 KEEPS LATENCIES CLOSE TO CASSANDRA STATS FOR 50th %tile

                    double seventyfive_percentile_current_run = StatUtils.percentile(target, 75);
                    seventyfive_read_latency.add(seventyfive_percentile_current_run);
                    double seventyfivePercentile_over_all_runs = StatUtils.percentile(toPrimitive(seventyfive_read_latency.toArray(new Double[seventyfive_read_latency.size()])), 45);//45 KEEPS LATENCIES CLOSE TO CASSANDRA STATS FOR 75th %tile

                    double ninetyfive_percentile_current_run = StatUtils.percentile(target, 95);
                    ninetyfive_read_latency.add(ninetyfive_percentile_current_run);
                    //45 is good for 2500 for matching with datadog, lets try 50 for improvement
                    //50 is better for 2500 for matching with datadog, lets try 55 for improvement
                    //55 is better for 2500 for matching with datadog, lets try 60 for improvement
                    //60 is better for 2500 for matching with datadog, lets try 65 for improvement
                    //65 is better for 2500 for matching with datadog, lets try 70 for improvement
                    //70 is better for 2500 for matching with datadog, lets try 75 for improvement
                    //75 is better for 2500 for matching with datadog, lets try 80 for improvement
                    //80 is when divergence starts for 2500 for matching with datadog, must use <80 , lets try 78
                    //78 still not better - for 2500 for matching with datadog, lets try 77
                    //77 is perfect for 2500 for matching with datadog
                    double ninetyfivePercentile_over_all_runs = StatUtils.percentile(toPrimitive(ninetyfive_read_latency.toArray(new Double[ninetyfive_read_latency.size()])), 77); //50 KEEPS LATENCIES CLOSE TO CASSANDRA STATS FOR 95th %tile

                    double ninetynine_percentile_current_run = StatUtils.percentile(target, 99);
                    ninetynine_read_latency.add(ninetynine_percentile_current_run);
                    //95 is good for 2500 for matching with datadog, lets try 99
                    //99 is good for 2500 - starts to diverge from datadog - so stick with below 99
                    //96 is an improvement for 2500, lets stay here, lets try 97
                    //97 is an improvement for 2500, lets stay here, lets try 98
                    //98 is perfect for 2500 for matching with datadog
                    double ninetyninePercentile_over_all_runs = StatUtils.percentile(toPrimitive(ninetynine_read_latency.toArray(new Double[ninetynine_read_latency.size()])), 98); //95 KEEPS LATENCIES CLOSE TO CASSANDRA STATS FOR 99th %tile

                    System.out.println("min_read_latency: " + min_read_latency);
                    System.out.println("max_read_latency: " + max_read_latency);
                    System.out.println(operation + "50th %tile over all runs: " + fiftyPercentile_over_all_runs + " 50th %tile for current run: " + fifty_percentile_current_run);
                    System.out.println(operation + "75th %tile over all runs: " + seventyfivePercentile_over_all_runs + " 75th %tile for current run: " + seventyfive_percentile_current_run);
                    System.out.println(operation + "95th %tile over all runs: " + ninetyfivePercentile_over_all_runs + " 95th %tile for current run: " + ninetyfive_percentile_current_run);
                    System.out.println(operation + "99th %tile over all runs: " + ninetyninePercentile_over_all_runs + " 99th %tile for current run: " + ninetynine_percentile_current_run);
                }


            }
        }
    }

    private static void resetConsole() {
//            final String ANSI_CLS = "\u001b[2J";
//            final String ANSI_HOME = "\u001b[H";
//            System.out.print(ANSI_CLS + ANSI_HOME);
//            System.out.flush();

        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    private static String convertToStars(int num) {
        StringBuilder builder = new StringBuilder();
        if (num > 1000) num = num / 100;
        for (int j = 0; j < num; j++) {
            builder.append('*');
        }
        return builder.toString();
    }

    public static double[] toPrimitive(Double[] array) {
        if (array == null) {
            return null;
        } else if (array.length == 0) {
            return null;
        }
        final double[] result = new double[array.length];
        for (int i = 0; i < array.length; i++) {
            result[i] = array[i].doubleValue();
        }
        return result;
    }

    public static void printUptime() {
        long endTime = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println("uptime = " + formatTime(totalTime));
    }

    public static String formatTime(long millis) {
        return String.format("%d min_write_latency, %d sec",
                TimeUnit.MILLISECONDS.toMinutes(millis),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
        );
    }

}